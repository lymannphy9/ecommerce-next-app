import { Suspense } from "react";
import "../globals.css";
import NavbarComponent from "../../components/layouts/navbar/NavbarComponent";
import Loading from "./loading";
import Error from "./error";
import { ErrorBoundary } from "next/dist/client/components/error-boundary";
import { Kantumruy_Pro, Poppins } from "next/font/google";
import { Metadata } from "next";
import StoreProvider from "../StoreProvider";
import SessionWrapper from "../SessionProvider";
import { FooterComponent } from "../../components/layouts/footer/FooterComponent";

export const metadata: Metadata = {
  title: "ISTAD Ecommerce Web",
  description: "ISTAD Ecommerce Web is a web application for selling products.",
  openGraph: {
    title: "ISTAD Ecommerce Web",
    description:
      "ISTAD Ecommerce Web is a web application for selling products.",
    images: "https://store.istad.co/media/brand_images/sokea_AF6QosU.jpg",
  },
};

const poppins = Poppins({ 
  subsets: ["latin"],
  weight:["300", "400", "500", "600"],
  display:"swap",
  style: ['italic', 'normal'],
  variable: "--font-poppins",
})
const kantumruy_pro = Kantumruy_Pro({
  subsets: ["khmer"],
  display: "swap",
  variable: "--font-kantumruy-pro",
})

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="en">
      <SessionWrapper>
        <body
          className={`${poppins.variable} ${kantumruy_pro.variable}`}
        >
          <StoreProvider>
            <header  className="container mx-auto sticky top-0 z-50">
              <NavbarComponent />
            </header>
            <ErrorBoundary errorComponent={Error}>
              <Suspense fallback={<Loading />}>{children}</Suspense>
            </ErrorBoundary>
          </StoreProvider>
          <footer>
            <FooterComponent />
          </footer>
        </body>
      </SessionWrapper>
    </html>
  );
}
