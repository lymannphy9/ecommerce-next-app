import React from 'react'

const page = () => {
  return (
    
    
   <>
     <div className="2xl:mx-auto 2xl:container flex justify-center items-center md:py-12 py-9 xl:px-20 sm:px-6 px-4">
    <div className="xl:w-auto md:flex hidden flex-col space-y-6 xl:space-y-0 xl:flex-row justify-center items-center">
        <div className="flex justify-between w-full items-center space-x-6 xl:space-x-8">
            <div className="flex justify-center items-center">
                <img className="" src="https://i.ibb.co/YDKkv5H/heather-ford-5gk-Ysr-H-eb-Y-unsplash-1.png" alt="shoes and clothes" />
            </div>
            <div className="flex flex-col justify-between xl:mt-0 items-center space-y-6 xl:space-y-8">
                <div className="flex flex-col xl:flex-row justify-between h-full xl:justify-center items-center space-y-8 xl:space-y-0 xl:space-x-8">
                    <div className="md:w-72 mx-1 md:h-64 lg:mt-4 xl:mt-0 flex flex-col justify-center items-center text-center">
                        <p className="text-3xl dark:text-white xl:text-4xl font-semibold leading-7 xl:leading-9 text-center text-gray-800">Our Instagram</p>
                        <p className="text-base dark:text-gray-400 leading-6 mt-3 text-center text-gray-600">Follow us on instagram and tag us to get featured on our timeline</p>
                        <a href="javascript:void(0)" className=" dark:text-gray-400 focus:outline-none mt-4 focus:underline text-base leading-4 hover:underline text-center text-gray-800">@Ourinstaname</a>
                    </div>
                    <div className="">
                        <img className="hidden xl:block" src="https://i.ibb.co/XYPJ0pQ/nordwood-themes-Nv4-QHk-TVEa-I-unsplash-1.png" alt="jewellery" />
                        <img className="xl:hidden" src="https://i.ibb.co/b51F6gj/nordwood-themes-Nv4-QHk-TVEa-I-unsplash-1-1.png" alt="shoes and clothes" />
                    </div>
                </div>
                <div className="hidden xl:flex flex-row justify-center items-center space-x-6 xl:space-x-8">
                    <div className="">
                        <img className="" src="https://i.ibb.co/FD9ZHbF/camilla-carvalho-Y9dc-Qp-OIMHQ-unsplash-1.png" alt="jewellery" />
                    </div>
                    <div className="">
                        <img className="" src="https://i.ibb.co/KxxFD8R/jonathan-francisca-YHbcum51-JB0-unsplash-1.png" alt="watch" />
                    </div>
                </div>
            </div>
        </div>
        <div className="xl:hidden flex flex-row justify-between space-x-6 xl:space-x-8">
            <div className="">
                <img className="" src="https://i.ibb.co/FD9ZHbF/camilla-carvalho-Y9dc-Qp-OIMHQ-unsplash-1.png" alt="jewellery" />
            </div>
            <div className="">
                <img className="" src="https://i.ibb.co/KxxFD8R/jonathan-francisca-YHbcum51-JB0-unsplash-1.png" alt="watch" />
            </div>
        </div>
    </div>

    <div className="md:hidden flex justify-center items-center flex-col">
        <div className="w-80 flex flex-col justify-center items-center text-center">
            <p className="text-3xl lg:text-4xl dark:text-white font-semibold leading-7 lg:leading-9 text-center text-gray-800">Our Instagram</p>
            <p className="text-base leading-6 mt-3 dark:text-gray-400 text-center text-gray-600">Follow us on instagram and tag us to get featured on our timeline</p>
            <a href="javascript:void(0)" className="focus:outline-none dark:text-gray-400 mt-4 focus:underline text-base leading-4 hover:underline text-center text-gray-800">@Ourinstaname</a>
        </div>
        <div className="mt-8 flex flex-col justify-center space-y-4">
            <img src="https://i.ibb.co/dpQZWPz/heather-ford-5gk-Ysr-H-eb-Y-unsplash-1.png" alt="shoes and clothes" />
            <img src="https://i.ibb.co/b51F6gj/nordwood-themes-Nv4-QHk-TVEa-I-unsplash-1-1.png" alt="shoes and clothes" />
            <img src="https://i.ibb.co/2c03gv4/camilla-carvalho-Y9dc-Qp-OIMHQ-unsplash-1.png" alt="jewellery" />
            <img src="https://i.ibb.co/PDMYNxh/jonathan-francisca-YHbcum51-JB0-unsplash-1.png" alt="watch" />
        </div>
    </div>
</div>
<section className="text-gray-600 body-font">
  <div className="container px-5 py-24 mx-auto">
    <div className="flex flex-wrap -m-4">
      <div className="lg:w-1/3 lg:mb-0 mb-6 p-4">
        <div className="h-full text-center">
          <img alt="testimonial" className="w-20 h-20 mb-8 object-cover object-center rounded-full inline-block border-2 border-gray-200 bg-gray-100" src="assets/sokny.jpg" />
          <p className="leading-relaxed">Edison bulb retro cloud bread echo park, helvetica stumptown taiyaki taxidermy 90 cronut +1 kinfolk. Single-origin coffee ennui shaman taiyaki vape DIY tote bag drinking vinegar cronut adaptogen squid fanny pack vaporware.</p>
          <span className="inline-block h-1 w-10 rounded bg-indigo-500 mt-6 mb-4"></span>
          <h2 className="text-gray-900 font-medium title-font tracking-wider text-sm">HOLDEN CAULFIELD</h2>
          <p className="text-gray-500">Senior Product Designer</p>
        </div>
      </div>
      <div className="lg:w-1/3 lg:mb-0 mb-6 p-4">
        <div className="h-full text-center">
          <img alt="testimonial" className="w-20 h-20 mb-8 object-cover object-center rounded-full inline-block border-2 border-gray-200 bg-gray-100" src="assets/taingey.jpg" />
          <p className="leading-relaxed">Edison bulb retro cloud bread echo park, helvetica stumptown taiyaki taxidermy 90 cronut +1 kinfolk. Single-origin coffee ennui shaman taiyaki vape DIY tote bag drinking vinegar cronut adaptogen squid fanny pack vaporware.</p>
          <span className="inline-block h-1 w-10 rounded bg-indigo-500 mt-6 mb-4"></span>
          <h2 className="text-gray-900 font-medium title-font tracking-wider text-sm">ALPER KAMU</h2>
          <p className="text-gray-500">UI Develeoper</p>
        </div>
      </div>
      <div className="lg:w-1/3 lg:mb-0 p-4">
        <div className="h-full text-center">
          <img alt="testimonial" className="w-20 h-20 mb-8 object-cover object-center rounded-full inline-block border-2 border-gray-200 bg-gray-100" src="assets/lymann.jpg" />
          <p className="leading-relaxed">Edison bulb retro cloud bread echo park, helvetica stumptown taiyaki taxidermy 90 cronut +1 kinfolk. Single-origin coffee ennui shaman taiyaki vape DIY tote bag drinking vinegar cronut adaptogen squid fanny pack vaporware.</p>
          <span className="inline-block h-1 w-10 rounded bg-indigo-500 mt-6 mb-4"></span>
          <h2 className="text-gray-900 font-medium title-font tracking-wider text-sm">HENRY LETHAM</h2>
          <p className="text-gray-500">CTO</p>
        </div>
      </div>
    </div>
  </div>
</section>
   </>


  )
}

export default page
