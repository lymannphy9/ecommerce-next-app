"use client";
// import CategoryComponent from "@/components/CategoryComponent/CategoryComponent";
import { CartProductType } from "../../../lib/definitions";
import { useGetProductsQuery } from "../../../redux/service/product";
import { useState } from "react";
import ResponsivePagination from "react-responsive-pagination";
import "react-responsive-pagination/themes/classic.css";
import CardComponent from "../../../components/card/CardComponent";
import Link from "next/link";

export default function Home() {
  const [currentPage, setCurrentPage] = useState(1);
  const { data } = useGetProductsQuery({
    page: currentPage,
    pageSize: 10,
  });
  const products = data?.results ?? [];
  const totalPages = 5;
  return (
    <main>
      <div className="w-[90%] mx-auto text-3xl text-center font-semibold py-6 text-green-600">
        Our Collection
      </div>
      <section className="w-[90%] mx-auto grid grid-cols-1 md:grid-cols-2 lg:grid-cols-4 gap-5 my-[40px] ">
        {products.map((pro: CartProductType) => {
          return (
            <CardComponent 
              id={pro.id}
              key={pro.id}
              name={pro.name}
              image={pro.image}
              price={pro.price}
              desc={pro.desc}
            />
          );
        })}
      </section>
      <section className="my-10">
        <ResponsivePagination
          current={currentPage}
          total={totalPages}
          onPageChange={setCurrentPage}
        />
      </section>
    </main>
  );
}
