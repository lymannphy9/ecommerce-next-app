"use client";

import React, { useState } from "react";
import style from "../login/style.module.css";
import { useSession, signIn, signOut } from "next-auth/react";
import { FcGoogle } from "react-icons/fc";
import { FaGithub } from "react-icons/fa";


import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from "yup";
import { useRouter } from "next/navigation";
type ValueTypes = {
	email: string;
	password: string;
};

const initialValues: ValueTypes = {
	email: "",
	password: "",
};


const validationSchema = Yup.object().shape({
	email: Yup.string().email("Invalid email").required("Required"),
	password: Yup.string().required("Required"),
});

export default function Login() {
	const [showPassword, setShowPassword] = useState(false);
	const [loading, setLoading] = useState(false);
	const route = useRouter();
	// const handleShowPassword = () => {
	// 	setShowPassword(!showPassword);
	// 	// Toggle password visibility
	// };

	//  handle submit
	const handleSubmit = (values: ValueTypes) => {
		setLoading(true);
		fetch(`https://store.istad.co/api/user/login/`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
			},
			body: JSON.stringify(values),
		}).then((res) => res.json()).then((data) => {
			console.log(data);
			setLoading(false);
			// route.push("/");
		}).catch((error) => {
			console.log(error);
			setLoading(false);
		});
	};

	if (loading) {
		return (
			<div className={`${style.container}`}>
				<h1 className="text-6xl text-center">Loading...</h1>
			</div>
		);
	}

	return (
		<main>
			<Formik
				initialValues={initialValues}
				validationSchema={validationSchema}
				onSubmit={(values, actions) => {
					handleSubmit(values);
				}}
			>
		
					<div className=" bg-gray-100 text-gray-900 flex justify-center">
						<div className="m-0 sm:m-10 bg-white shadow sm:rounded-lg flex justify-center ">
							<div className="lg:w-1/2 xl:w-5/12 p-6 sm:p-12">
								<a href="/">
									<img
										src="/assets/green-market-logo.png"
										alt="Logo"
										className="w-32 mx-auto"
									/>
								</a>
								<div className="mt-12 flex flex-col items-center">
									<h1 className="text-2xl xl:text-3xl font-extrabold">
										Sign up
									</h1>
									<div className="w-full flex-1 mt-8">
										<div className="flex flex-col items-center">
											<button className="w-full max-w-xs font-bold shadow-sm rounded-lg py-3 bg-indigo-100 text-gray-800 flex items-center justify-center transition-all duration-300 ease-in-out focus:outline-none hover:shadow focus:shadow-sm focus:shadow-outline" onClick={() => signIn("google")}>
												<div className="bg-white p-2 rounded-full">
												<FcGoogle/>
												</div>
												<span className="ml-4">
													Sign Up with Google
												</span>
											</button>

											<button className="w-full max-w-xs font-bold shadow-sm rounded-lg py-3 bg-indigo-100 text-gray-800 flex items-center justify-center transition-all duration-300 ease-in-out focus:outline-none hover:shadow focus:shadow-sm focus:shadow-outline mt-5" onClick={() => signIn("github")}>
												<div className="bg-white p-1 rounded-full">
												<FaGithub />
												</div>
												<span className="ml-4">
													Sign Up with GitHub
												</span>
											</button>
										</div>

										<div className="my-12 border-b text-center">
											<div className="leading-none px-2 inline-block text-sm text-gray-600 tracking-wide font-medium bg-white transform translate-y-1/2">
												Or sign up with e-mail
											</div>
										</div>
										<Form>
										<div className="mx-auto max-w-xs">
											<Field
												type="email"
												name="email"
												id="email"
												className="w-full px-8 py-4 rounded-lg font-medium bg-gray-100 border border-gray-200 placeholder-gray-500 text-sm focus:outline-none focus:border-gray-400 focus:bg-white"
												placeholder="Email"
											/>
											<ErrorMessage
												name="email"
												component="section"
												className={`${style.error}`}
											/>
											<Field
												type={showPassword ? "text" : "password"}
												name="password"
												id="password"
												className="w-full px-8 py-4 rounded-lg font-medium bg-gray-100 border border-gray-200 placeholder-gray-500 text-sm focus:outline-none focus:border-gray-400 focus:bg-white mt-5"
												placeholder="Password"
											/>
											<ErrorMessage
												name="password"
												component="section"
												className={`${style.error}`}
											/>

											<button
												className="mt-5 tracking-wide font-semibold bg-green-600 text-gray-100 w-full py-4 rounded-lg hover:bg-green-400 transition-all duration-300 ease-in-out flex items-center justify-center focus:shadow-outline focus:outline-none"
												type="submit"
											>
												<svg className="w-6 h-6 -ml-2" fill="none" stroke="currentColor" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round">
													{/* Sign Up SVG paths */}
												</svg>
												<span className="ml-3">
													Sign Up
												</span>
											</button>
											<p className="mt-6 text-xs text-gray-600 text-center">
												I agree to abide by templatana
												<a href="#" className="border-b border-gray-500 border-dotted">
													Terms of Service
												</a>
												and its
												<a href="#" className="border-b border-gray-500 border-dotted">
													Privacy Policy
												</a>
											</p>
										</div>
										</Form>
									</div>
								</div>
							</div>
							<div className="flex-1 text-center hidden lg:flex">
								<div className="m-12 xl:m-16 w-full bg-contain bg-center bg-no-repeat">
									<img
										src="/assets/login1.jpg"
										layout="fill"
										alt="Illustration"
										
									/>
								</div>
							</div>
						</div>
					</div>
			</Formik>
		</main>
	);
}


