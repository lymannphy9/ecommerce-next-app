import React from 'react'
import { HeaderComponent } from './homepage/HeaderComponent'
import ProductCard from '../app/(user)/product/page'
import BrandComponent from './homepage/BrandComponent'
import CategoryComponent from './homepage/CategoryComponent'

export const HomePageComponent = () => {
  return (
    <div className='container'>
      <HeaderComponent/>
      <BrandComponent/>
      <CategoryComponent/>
      <ProductCard/>
    </div>
  )
}
