import React from 'react'
import { ProductDetailType } from '../../lib/definitions'

export const CardProductDetail = ({name, desc, price, image} : ProductDetailType) => {
  return (
    <div className="2xl:container 2xl:mx-auto lg:py-16 lg:px-20 md:py-12 md:px-6 py-9 px-4">
            <div className="flex justify-center items-center lg:flex-row flex-col gap-8">
       
                <div className="w-full sm:w-96 md:w-8/12 lg:w-6/12 items-center">
                    <p className="focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-gray-800 font-normal text-base leading-4 text-gray-600  dark:text-white">Home / Furniture / Wooden Stool</p>
                    <h2 className="font-semibold lg:text-4xl text-3xl lg:leading-9 leading-7 text-gray-800 dark:text-white mt-4">{name}</h2>
      
                    <div className="flex flex-row justify-between mt-5">
                       <img className="dark:hidden" src="https://tuk-cdn.s3.amazonaws.com/can-uploader/productDetail4-svg1.svg" alt="stars" />
                       <img className="hidden dark:block" src="https://tuk-cdn.s3.amazonaws.com/can-uploader/productDetail4-svg1dark.svg" alt="stars" />
                        <p className="focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-gray-800 font-normal text-base leading-4 text-gray-700 hover:underline hover:text-gray-800 dark:text-white duration-100 cursor-pointer">22 reviews</p>
                    </div>
      
                    <p className="font-normal text-base leading-6 text-gray-600  mt-7">{desc}</p>
                    <p className="font-semibold lg:text-2xl text-xl lg:leading-6 leading-5 mt-6 dark:text-white">$ {price}</p>
      
                    <div className="lg:mt-11 mt-10">
                        <div className="flex flex-row justify-between">
                            <p className="font-medium text-base leading-4 text-gray-600 ">Select quantity</p>
                            <div className="flex">
                                <span className="focus:outline-none dark:text-white focus:ring-2 focus:ring-offset-2 focus:ring-gray-800 cursor-pointer border border-gray-300 border-r-0 w-7 h-7 flex items-center justify-center pb-1">-</span>
                                <input id="counter" aria-label="input" className="border dark:text-white border-gray-300 dark:bg-transparent h-full text-center w-14 pb-1" type="text" value="1" />
                                <span className="focus:outline-none dark:text-white focus:ring-2 focus:ring-offset-2 focus:ring-gray-800 cursor-pointer border border-gray-300 border-l-0 w-7 h-7 flex items-center justify-center pb-1">+</span>
                            </div>
                        </div>
                        <hr className="bg-gray-200 w-full my-2" />
                        <div className="flex flex-row justify-between items-center mt-4">
                            <p className="font-medium text-base leading-4 text-gray-600 ">Dimensions</p>
                            <img id="rotateSVG" className="focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-gray-800 cursor-pointer transform duration-100  dark:hidden" src="https://tuk-cdn.s3.amazonaws.com/can-uploader/svg4.svg" alt="dropdown" />
                            <img id="rotateSVG" className="focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-gray-800 cursor-pointer transform duration-100 hidden dark:block" src="https://tuk-cdn.s3.amazonaws.com/can-uploader/svg4dark.svg" alt="dropdown" />
                        </div>
                        <hr className="bg-gray-200 w-full mt-4" />
                    </div>
      
                    <button className="focus:outline-none focus:ring-2 hover:bg-black focus:ring-offset-2 focus:ring-gray-800 font-medium text-base leading-4 text-white bg-green-600 w-full py-5 lg:mt-12 mt-6">Add to Cart</button>
                </div>
    
                <div className="w-full lg:w-6/12 bg-gray-100 flex justify-center items-center">
                        <img src={image} className="w-[400px] h-[500px] rounded-lg" alt="Product View Details" />
                    </div>
                </div>
            </div>


  )
}
