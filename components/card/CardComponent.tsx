"use client";

import Image from "next/image";
import { ProductType } from "../../lib/definitions";
import { addToCart } from "../../redux/features/cart/cartSlice";
import { useAppDispatch } from "../../redux/hooks";
import Link from "next/link";


export default function CardComponent({
    id,
    name,
    image,
    price,
    desc,
    onClick
}: ProductType) {
    const dispatch = useAppDispatch();
    const limitedDesc = desc.slice(0, 20) + (desc.length > 20 ? "..." : "");
    const handleAddToCart = () => {
        dispatch(
          addToCart({
            id: id,
            name: name,
            image: image,
            price: price,
            desc: desc,
            // category: category,
            quantity: 1
          })
        );
      };
    
    return (
        <div className="container mx-auto bg-white rounded-lg overflow-hidden shadow-lg max-w-sm">
            <div className="relative">
            <Link href={`/product/${id}`} passHref>
                <img src={image} alt="product-img" className="w-full h-[400px] object-cover"/>
            </Link>
                <div className="absolute top-0 right-0 bg-red-500 text-white px-2 py-1 m-2 rounded-md text-sm font-medium">SALE</div>
            </div>
            <div className="p-4">
                <h3 className="text-lg font-medium mb-2">{name}</h3>
                <p className="text-gray-600 text-sm mb-4">{limitedDesc}</p>
                <div className="flex items-center justify-between">
                    <span className="font-bold text-lg">${price}</span>
                    <button onClick={handleAddToCart} className="bg-green-600 hover:bg-green-400 text-white font-bold py-2 px-4 rounded">Add to</button>
                </div>
            </div>
        </div>
    );
}
