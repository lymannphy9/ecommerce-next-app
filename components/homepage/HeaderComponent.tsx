import Image from 'next/image'
import React from 'react'

export const HeaderComponent = () => {
    
    return (
        <div className='w-[90%] mx-auto '>
            <div
                className="relative dark:bg-dark pt-[120px] pb-[110px] lg:pt-[150px] -z-20"
            >
                <div className="container mx-auto ">
                    <div className="flex flex-wrap items-center -mx-4">
                        <div className="w-full px-4 lg:w-5/12 ">
                            <div className="hero-content">
                                <h1
                                    className="mb-5 text-4xl font-bold !leading-[1.208] text-dark font-poppins dark:text-white sm:text-[42px] lg:text-[40px] xl:text-5xl"
                                >
                                    The Greatest <br />
                                    Journey Of Online <br />
                                    Payment.
                                </h1>
                                <p
                                    className="mb-8 max-w-[480px] text-base text-body-color"
                                >
                                    With TailGrids, business and students thrive together. Business
                                    can perfectly match their staffing to changing demand throughout
                                    the dayed.
                                </p>
                                <ul className="flex flex-wrap items-center">
                                    <li>
                                        <a
                                            href="javascript:void(0)"
                                            className="inline-flex items-center justify-center px-6 py-3 text-base font-medium text-center text-white rounded-md bg-green-600 hover:bg-blue-dark lg:px-7"
                                        >
                                            Get Started
                                        </a>
                                    </li>
                                </ul>
                               
                            </div>
                        </div>
                        <div className="hidden px-4 lg:block lg:w-1/12"></div>
                        <div className="w-full px-4 lg:w-6/12">
                            <div className="lg:ml-auto lg:text-right">
                                <div className="relative z-10 inline-block pt-11 lg:pt-0">
                                    <img
                                        src="https://img.freepik.com/free-photo/front-view-young-saleswoman-holding-gift-packages-green-surface_140725-154747.jpg?t=st=1714710148~exp=1714713748~hmac=4bc0f6edf57e5f1869b467de6db60dae1652bce5f479f676f68009fccbf229b2&w=1060"
                                        alt="hero"
                                        className="w-[700px] opacity-100 lg:ml-auto rounded-lg"
                                    />
                                    <span className="absolute -left-8 -bottom-8 z-[-1]">
                                        <svg
                                            width="93"
                                            height="93"
                                            viewBox="0 0 93 93"
                                            fill="none"
                                            xmlns="http://www.w3.org/2000/svg"
                                        >
                                            <circle cx="2.5" cy="2.5" r="2.5" fill="#008000" />
                                            <circle cx="2.5" cy="24.5" r="2.5" fill="#008000" />
                                            <circle cx="2.5" cy="46.5" r="2.5" fill="#008000" />
                                            <circle cx="2.5" cy="68.5" r="2.5" fill="#008000" />
                                            <circle cx="2.5" cy="90.5" r="2.5" fill="#008000" />
                                            <circle cx="24.5" cy="2.5" r="2.5" fill="#008000" />
                                            <circle cx="24.5" cy="24.5" r="2.5" fill="#008000" />
                                            <circle cx="24.5" cy="46.5" r="2.5" fill="#008000" />
                                            <circle cx="24.5" cy="68.5" r="2.5" fill="#008000" />
                                            <circle cx="24.5" cy="90.5" r="2.5" fill="#008000" />
                                            <circle cx="46.5" cy="2.5" r="2.5" fill="#008000" />
                                            <circle cx="46.5" cy="24.5" r="2.5" fill="#008000" />
                                            <circle cx="46.5" cy="46.5" r="2.5" fill="#008000" />
                                            <circle cx="46.5" cy="68.5" r="2.5" fill="#008000" />
                                            <circle cx="46.5" cy="90.5" r="2.5" fill="#008000" />
                                            <circle cx="68.5" cy="2.5" r="2.5" fill="#008000" />
                                            <circle cx="68.5" cy="24.5" r="2.5" fill="#008000" />
                                            <circle cx="68.5" cy="46.5" r="2.5" fill="#008000" />
                                            <circle cx="68.5" cy="68.5" r="2.5" fill="#008000" />
                                            <circle cx="68.5" cy="90.5" r="2.5" fill="#008000" />
                                            <circle cx="90.5" cy="2.5" r="2.5" fill="#008000" />
                                            <circle cx="90.5" cy="24.5" r="2.5" fill="#008000" />
                                            <circle cx="90.5" cy="46.5" r="2.5" fill="#008000" />
                                            <circle cx="90.5" cy="68.5" r="2.5" fill="#008000" />
                                            <circle cx="90.5" cy="90.5" r="2.5" fill="#008000" />
                                        </svg>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    )
}
