import { ecommerceApi } from "../api"; 

// Define a service using a base URL from the "ecommerceApi" and injects endpoints to it
export const productApi = ecommerceApi.injectEndpoints({
    endpoints: (builder) => ({
        // get all products
        //                        <result type,         args type>
        getProducts: builder.query<any, { page: number; pageSize: number }>({
            query: ({ page = 1, pageSize = 10 }) =>
                `api/products/?page=${page}&page_size=${pageSize}`,
        }),

        // get single product
        getProductById: builder.query<any, number>({
            query: (id) => `products/${id}/`,
        }), 

        // update a product
        updateProduct: builder.mutation<
            any,
            { id: number; updatedProduct: object; accessToken: string }
        >({
            query: ({ id, updatedProduct, accessToken }) => ({
                url: `api/products/${id}/`,
                method: "PATCH",
                headers: {
                    "Content-Type": "application/json",
                    Authorization: `Bearer ${accessToken}`,
                },
                body: updatedProduct,
            }),
        }),

    }),
    overrideExisting: false, // don't override existing hooks
});
// Export hooks for usage in components, which are
export const {
    useGetProductsQuery,
    useGetProductByIdQuery,
    useUpdateProductMutation,
} = productApi;
