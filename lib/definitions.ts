export type ProductType = {
	id: number;
	name: string;
	price: number;
	desc: string;
	image: string;
	quantity?: number;
	onClick?: () => void;
  };
  
  
  export type CartProductType = {
	category: string;
	image: string;
	price: number;
	name: string;
	id: number;
	desc: string;
	onClick?: () => void;
  };

  export type ProductDetailType = {
	id?: string,
    name: string,
    desc: string,
    image: string,
    price: number,
  }
  